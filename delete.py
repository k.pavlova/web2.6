#!/usr/bin/env python3

import os
import sys
import cgi
import cgitb
import codecs
import html
import http.cookies

import psycopg2
from psycopg2 import sql

form = cgi.FieldStorage()

username = form.getvalue('field-username')
f = False
conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    try:
        insert = sql.SQL(f"DELETE FROM form WHERE first_name = '{username}'")
        cursor.execute(insert)
        f = True
    except psycopg2.Error:
        f = False
conn.close()

if f:
    print("Content-type: text/html")
    print()
    print('''
        <!DOCTYPE html>
    <html lang="ru">
        <head>
            <meta charset="UTF-8">
            <title>Авторизация</title>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        </head>
        <body>
            <h1>Запись удалена</h1>
            <a href="admin.py" class="btn btn-success">Назад в админку</a>
        </body>
    </html>
    ''')
